<?php

namespace App\Http\Controllers\JWTAuth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use JWT;
use Config;
use Auth;


class AuthenticateController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $user = JWTAuth::parseToken()->toUser();
      return response()->json($user);
    }

    public function authenticate(Request $request)
    {
        // $credentials = $request->only('email', 'password');
        $user = Auth::user();

        $customClaims = [
          'name'  => $user->name,
          'email' => $user->email
        ];


        try {
            // // verify the credentials and create a token for the user
            // if (! $token = JWTAuth::attempt($credentials)) {
            if ( !$token = JWTAuth::fromUser($user, $customClaims) ) {
              return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token', $e->getMessage()], 500);
        }

        // if no errors are encountered we can return a JWT
        $token = $this->standardizeJWTToken($token);
        return response()->json([
          'token' => $token,
          'email' => $user->email
        ]);
    }


    private function standardizeJWTToken($token) {
      $key  = Config::get('jwt.secret');
      $algo = Config::get('jwt.algo');

      $decoded = JWT::decode($token, $key, [$algo]);

      $decoded->nbf = (int)$decoded->nbf;
      $decoded->iat = (int)$decoded->iat;
      $decoded->exp = (int)$decoded->exp;

      return JWT::encode($decoded, $key);
    }
}
