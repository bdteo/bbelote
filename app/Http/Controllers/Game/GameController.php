<?php
namespace App\Http\Controllers\Game;

use Session;
use Redis;
use App\Http\Controllers\Controller;

class GameController extends Controller {
  public function initGame() {
    setcookie(
      'gameKey',
      Session::getId(),
      time() + 24 * 3600
    );
    return response()->view('game.game');
  }
}
