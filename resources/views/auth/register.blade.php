@extends('master')

@section('title', 'Регистрация')

@section('content')
<form method="POST" action="/auth/register" class="form-signin-register">
  {!! csrf_field() !!}
  <h2 class="form-signin-heading">Регистрация</h2>
  Име <input type="text" name="name" class="input-block-level" value="{{ old('name') }}">
  Имейл <input type="email" name="email" class="input-block-level" value="{{ old('email') }}">
  Парола <input type="password" class="input-block-level" name="password">
  Повторете паролата <input type="password" class="input-block-level" name="password_confirmation">
  <button class="btn btn-large btn-primary" type="submit">Регистрация</button>
</form>
@endsection
