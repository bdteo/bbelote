@extends('master')

@section('title', 'Вход')

@section('content')

<form method="POST" action="/auth/login" class="form-signin-register">
  {!! csrf_field() !!}
  <h2 class="form-signin-heading">Вход</h2>
  <input type="text" name="email" class="input-block-level" placeholder="Email адрес">
  <input type="password" name="password" class="input-block-level" placeholder="Парола">
  <label class="checkbox">
    <input type="checkbox" name="remember" value="remember-me"> Запомни ме
  </label>
  <button class="btn btn-large btn-primary" type="submit">Вход</button>
</form>
@endsection
