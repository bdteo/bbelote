<script src="/bower_components/socket.io-client/socket.io.js"></script>
<script src="/bower_components/angularjs/angular.min.js"></script>
<script src="/bower_components/angular-socket-io/socket.min.js"></script>
<script src="/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/bower_components/snap.svg/dist/snap.svg-min.js"></script>
<script src="/app/app.module.js"></script>
<script src="/app/app.routes.js"></script>
<script src="/app/components/lobby/LobbyController.js"></script>
<script src="/app/components/game/gameFront.js"></script>
<div ng-app="bbeloteApp">
  <div ng-controller="LobbyController">
    <div class="panel panel-default well content" ng-show="!roomJoined">
      <button type="button" class="btn btn-primary" ng-click="create()">
        Създай стая
      </button>
    </div>
    
    <div class="panel panel-default well content gameContainer" ng-show="roomJoined" ng-controller="GameController">
      <h3>Игра</h3>
      <button type="button" class="btn btn-primary" ng-click="startGame()">Стартирай игра</button>
      <br>
      <br>
      <game>
				
        <div class="myCards">
          <div class="card" ng-repeat="(key, card) in myCards" ng-click="playCard(card)">
            <img ng-src="{{card.imageUrl}}">
          </div>
        </div>
        
        
        <div class="card card{{id}}" ng-repeat="id in [0,1,2,3]">
           <img ng-if="cardsOnTable[id]" ng-src="{{cardsOnTable[id].imageUrl}}">
        </div>
      </game>
    </div>    
    
    <div class="panel panel-default well content chatPanel" ng-show="roomJoined">
      <h3>Чат</h3>
      <div class="messages well">
        <div ng-repeat="message in messages">
          <strong><em>{{message.author}}: </em></strong>
          {{message.data}}
        </div>
      </div>
      <input type="text" class="messageInput well" ng-model="chatInput" ng-keypress="$event.keyCode == 13 && chat(chatInput)">
      &nbsp;&nbsp;
      <button type="button" class="btn btn-primary" ng-if="!room.joined" ng-click="chat(chatInput)">Изпрати</button>
    </div>
    
    <div ng-repeat="(id, room) in visibleRooms" class="panel panel-default well content">
      <h4>Стая {{id}}</h4>
      <ul>
        <li ng-repeat="(userId, user) in room.users">{{userId}}</li>
      </ul>
      <button type="button" class="btn btn-primary" ng-if="!room.joined" ng-click="join(id)">
        Влез
      </button>
      <button type="button" class="btn btn-default" ng-if="room.joined" ng-click="leave(id)">
        Излез
      </button>
    </div>  
    
  </div>
</div>
