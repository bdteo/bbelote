<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('title') - Онлайн Белот</title>

    <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="container">
      <div class="header">
        <div class="header-top">
          <div class="header-text"></div>
          <div class="header-image"><img src="/assets/img/header-cards-100.png"></div>
        </div>
        <div class="header-bottom"></div>
      </div>
      <nav class="navbar navbar-default navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Онлайн Белот</a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">Начало</a></li>
              <li class="{{ Request::is('/game/') ? 'active' : '' }}"><a href="/game/">Игра</a></li>


              @if (!Auth::check())              
              <li class="{{ Request::is('/auth/register/') ? 'active' : '' }}"><a href="/auth/register/">Регистрация</a></li>
              @endif
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
              @if (Auth::check())
              <li><a href="#">{{Auth::user()->email}}</a></li>
              <li><a href="/auth/logout/">Изход</a></li>
              @else
              <li class="{{ Request::is('/auth/login/') ? 'active' : '' }}"><a href="/auth/login/">Вход</a></li>
              @endif
            </ul>
            
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

      @yield('content')
    </div><!-- /.container -->
  </body>
</html>
