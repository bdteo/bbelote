'use strict';

angular.module('bbeloteApp.lobby').controller('LobbyController', ['$scope', '$timeout', 'gameSocketPromise', function controller($scope, $timeout, gameSocketPromise) {

  gameSocketPromise.getSocket().then(function onAuthenticated(data) {
    var socket = data.socket;

    $scope.roomJoined = null;

    $scope.visibleRooms = $scope.rooms = {};


    $scope.join   = function join(id) {
      //console.log(id);
      socket.emit('joinRoom', id);
    };

    $scope.leave  = function leave(id) {
      //console.log(id);
      socket.emit('leaveRoom', id);
    };

    $scope.create = function create() {
      socket.emit('createRoom');
    };

    $scope.chat   = function chat(message) {
      socket.emit('chat', message);
      $timeout(function () {
        $scope.chatInput = '';
      });
    };


    $scope.$on('socket:roomCreated', function (event, data) {
      $timeout(function() {
        $scope.rooms[data.id] = data;
        updateJoined();
      });
    });

    $scope.$on('socket:roomDestroyed', function (event, id) {
      //console.log('roomDestroyed', id);
      $timeout(function() {
        delete $scope.rooms[id];
        updateJoined();
      });
    });

    $scope.$on('socket:roomJoin', function (event, data) {
      //console.log('roomJoin', data.id);
      $timeout(function() {
        if ($scope.rooms[data.id]) {
          if (!$scope.rooms[data.id].users) {
            $scope.rooms[data.id].users = {};
          }

          $scope.rooms[data.id].users[data.user] = {};

          updateJoinedForRoom($scope.rooms[data.id]);
          
          
          //$scope.myCards = {};//todo: do not mix controller logic
        }
      });
    });

    $scope.$on('socket:roomLeave', function (event, data) {
      //console.log('roomLeave', data.id);
      $timeout(function () {
        if (
					$scope.rooms[data.id]
					&& $scope.rooms[data.id].users[data.user]
				) {
          delete $scope.rooms[data.id].users[data.user];
          updateJoinedForRoom($scope.rooms[data.id]);
        }
      });
    });


    function updateJoinedForRoom(room) {
      var you    = room.you;
      var joined = typeof room.users[you] !== 'undefined';

      room.joined = joined;

      updateJoined();
    }

    function updateJoined() {
      var room;

      var roomJoined = null;

      for (var id in $scope.rooms) {
        room = $scope.rooms[id];
        if (room.joined) {
          roomJoined = room;
          break;
        }
      }

      $scope.roomJoined = roomJoined;

      if (roomJoined) {
        $scope.visibleRooms = {};
        $scope.visibleRooms[roomJoined.id] = roomJoined;
      }
      else {
        $scope.visibleRooms = $scope.rooms;
      }
    }


    /*socket.on('leaved', function (data) {console.log('leaved', data);});
    socket.on('joined', function (data) {console.log('joined', data);});

    socket.on('youJoined', function (data) {console.log('youJoined', data);});

    socket.on('userList', function (data) {console.log('userList', data);});*/

    $scope.$on('socket:roomList', function (event, roomList) {

      $timeout(function() {
        var data;

        $scope.rooms = {};
        for (var i = 0, l = roomList.length; i<l; i++) {
          data = roomList[i];
          $scope.rooms[data.id] = data;
        }

        $scope.visibleRooms = $scope.rooms;
      });
    });


    $scope.messages = [];
    $scope.$on('socket:chat', function (event, message) {
      $scope.messages.push(message);
    });

    //socket.emit('createRoom');
    socket.emit('getRoomList');
  });

}]);
