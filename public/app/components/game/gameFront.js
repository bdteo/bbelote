'use strict';
var sc;
(function () {

var suits = {
  "clubs"   : 1,
  "diamonds" : 2,
  "hearts"   : 3,
  "spades"   : 4
};

var ranks = {
  "7"  : 1,
  "8"  : 2,
  "9"  : 3,
  "j"  : 4,
  "q"  : 5,
  "k"  : 6,
  "10" : 7,
  "a"  : 8
};

angular.module('bbeloteApp.game').controller('GameController', ['$scope', '$timeout', 'gameSocketPromise', function controller($scope, $timeout, gameSocketPromise) {
  $scope.myCards      = {};
  $scope.cardsOnTable = {};

  gameSocketPromise.getSocket().then(function onAuthenticated(data) {$timeout(function (){
    var socket = data.socket;
    var email  = data.email;

    sc = socket;

    $scope.playCard = function playCard(card) {      
      socket.emit('communicate', {event:'playCard', payload: card});      
    };

    $scope.startGame = function startGame(card) {
      socket.emit('communicate', {event:'startGame', payload: ''});
    };
    
    socket.on('yourTurn', function () {
      console.log('yourTurn');
    });
    
    
    socket.on('cardPlayed', function cardPlayed(data) {
			var card      = data.card,
			    player    = data.player,
					key       = card.rank + card.suit,
					order     = data.order,
					playerNum = order.indexOf(player),
					myNum     = order.indexOf(email);

			var numRotated = (order.length + playerNum - myNum) % order.length;
			
			card.imageUrl = getImageURLForCard(card);			

			console.log(data);////////
      $timeout(function () {
        delete $scope.myCards[key];
        console.log(numRotated);
        $scope.cardsOnTable[numRotated] = card;
      });      
    });

    /*socket.on('handWinner', function (data) {
      console.log(data);
    });    

    socket.on('receivesCard', function (data) {
      if (email !== data.player) {
        console.log(data);
      }
    });*/


    socket.on('receiveCard', function (card) {
      $timeout(function () {
        var key = card.rank + card.suit;
        card.imageUrl = getImageURLForCard(card);
        $scope.myCards[key] = card;
      });
    });
    
    socket.on('roomJoin', function (data) {
      $timeout(function () {
        if (data.user === email) {
					$scope.myCards      = {};
					$scope.cardsOnTable = {};
				}
      });
    });    
    
  });
  });
  
  
  function getImageURLForCard(card) {
		return '/assets/img/cards-separate/' + card.rank.toLowerCase() + "-of-" + card.suit.toLowerCase() +".svg";
	}
	
}])
// .directive('game', ['$timeout', 'gameSocketPromise', function factory($timeout, gameSocketPromise) {
  // return {
    // restrict    : 'E',
    // template    : '<svg></svg>',
    // scope: {
    // },
    // link        : function gameDirectiveLink(scope, element, attributes) {
      // var div = element.find('svg')[0];
      // var s = Snap(div);
      // s.attr({ viewBox: "0 0 1800 1300" });

      // var images = {};

      // for (var suit in suits) {
        // for (var rank in ranks) {
          // var key = rank + suit;

          // (function (key) {
            // Snap.load("/assets/img/cards-separate/" + rank + "-of-" + suit +".svg", function (image) {
              // images[key] = image;

              // if (Object.keys(images).length === 32) {
                // onImagesLoaded(s, images, gameSocketPromise);
              // }
            // });
          // })(key);
        // }
      // }
    // }
  // }
// }]);


// function onImagesLoaded(s, images, gameSocketPromise) {

  // var i = 0;

  // /*for (var id in images) {
    // var img = images[id].select('#root');
    // if (!img) continue;
    // img.transform('t' + i*50 + ',1180');
    // s.append(img);
    // ++i;
  // }*/


  // gameSocketPromise.getSocket().then(function onAuthenticated(data) {
    // var socket = data.socket;
    // var email  = data.email;

    // sc = socket;

    // socket.on('handWinner', function (data) {
      // console.log(data);
    // });

    // socket.on('yourTurn', function (data) {
      // console.log(data);
    // });

    // socket.on('receivesCard', function (data) {
      // if (email !== data.player) {
        // console.log(data);
      // }
    // });

    // socket.on('receiveCard', function (card) {
      // var key   = card.rank.toLocaleLowerCase() + card.suit.toLocaleLowerCase();

      // //console.log(key);
      // var image = images[key].select('#root').clone();

      // image.transform('t800,600');
      // s.append(image);


      // var x = card.num * 100 + 350,
          // y = 950,
          // rotation = card.num - 4;

      // image.animate({ transform: 't' + x + ',' + y + 'r' + rotation*2 +',' + 0 + ',' + 0 }, 500, mina.bounce );
    // });
  // });
// }

})();
