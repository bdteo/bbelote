'use strict';

angular.module('bbeloteApp.lobby', []);
angular.module('bbeloteApp.game', []);

angular.module('bbeloteApp', [
  'btford.socket-io',
  'bbeloteApp.lobby',
  'bbeloteApp.game',
  'ngAnimate'
])
.factory('gameSocketPromise', ['socketFactory', '$q', '$http', function factory(socketFactory, $q, $http) {
  var socket;
  var email;
  var promise;

  function getSocket() {
    if (promise) {
      return promise;
    }

    if (angular.isDefined(socket)) {
      promise = $q.when({
        socket : socket,
        email  : email
      });
    }
    else {
      var deferred = $q.defer();

      $http
      .post('/api/authenticate')
      .success(onTokenAcquired.bind(null, deferred));

      promise = deferred.promise
    }

    promise
    .catch(function (response) {
      return $q.reject(response.data);
    })
    .finally(function(){
      promise = null; //Just remove it here
    });

    return promise;

    function onTokenAcquired(deferred, JWTJson) {
      var s = io.connect('http://' + window.location.host + ':3002');

      //s.on('authenticated', function () {});

      //s.on('connect', function () {
      email = JWTJson.email;
      socket = socketFactory({
        ioSocket: s
      });

      socket.forward('roomCreated');
      socket.forward('roomDestroyed');
      socket.forward('roomJoin');
      socket.forward('roomLeave');

      socket.forward('roomList');

      socket.forward('leaved');
      socket.forward('joined');
      socket.forward('youJoined');

      socket.forward('userList');
      socket.forward('chat');

      s.emit('authenticate', JWTJson);

      deferred.resolve({
        socket : socket,
        email  : email
      });
      //});
    }
  };

  return {
    getSocket: getSocket
  };
}]);
