## Installation
on Ubuntu
- ssh-keygen
- sudo apt-get install VirtualBox
- download vagrant deb package and install vagrant from it
- install Homestead using Vagrant;
- cd ~; mkdir devEnvs; cd devEnvs
- git clone https://github.com/laravel/homestead.git Homestead
- bash init.sh
- vagrant up
- vagrant shh
... from Homestead virtual machine ssh:
- run "cd Code/bbelote/";
- run "cp .env.example .env"
- run "install composer";
- run "php artisan migrate";
- run "php artisan key:generate";
- run "serve bbelote.app /home/vagrant/Code/bbelote/public"
- run "cd ~/Code/bbelote/Node"
- run "npm start"
- open bbelote.app in browser
