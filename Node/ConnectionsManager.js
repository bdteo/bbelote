var uuid   = require('node-uuid');
var events = require('events');
var Game   = require('./Game.js');

/*Class*/
function ConnectionsManager() {
  var that = this;

  var connectionMediums = {};
  var connections       = {};

  (function __construct() {
    /*public methods*/
    that.addConnection = addConnection;
  })();


  function addConnection(connection) {
    if (connections[connection.decoded_token.email]) {
      return;
    }

    connections[connection.decoded_token.email] = connection;

    /*console.log(JSON.stringify(Object.keys(connections).map(function (v, k) {
      return connections[v].decoded_token.email;
    })));*/

    /*connection.on('error', function (err) {
      console.trace("trace ....");
    });*/

    connection.on('createRoom', handleCreateRoom.bind(null, connection));
    connection.on('joinRoom', handleJoinRoom.bind(null, connection));
    connection.on('leaveRoom', handleLeaveRoom.bind(null, connection));
    connection.on('getRoomList', handleGetRoomList.bind(null, connection));
    connection.on('disconnect', handleDisconnect.bind(null, connection));
  }


  function handleGetRoomList(connection) {
    connection.emit('roomList', getRoomList(connection));
  }


  function getRoomList(connection) {
    return Object.keys(connectionMediums).map(function (value, index) {
      var medium     = connectionMediums[value];
      var roomObject = makeRoomObject(medium);

      extendRoomObject(roomObject, connection);

      return roomObject;
    });
  }


  function makeRoomObject(medium) {
    return {
      id     : medium.getId(),
      users  : medium.getUserList()
    };
  }


  function extendRoomObject(roomObject, connection) {
    var you    = connection.decoded_token.email,
        joined = typeof roomObject.users[you] !== 'undefined';

    roomObject.you    = you;
    roomObject.joined = joined;
  }


  function handleCreateRoom(connection) {
    var medium = new ConnectionMedium();
    var id     = medium.getId();
    connectionMediums[id] = medium;

    medium.on('mediumEmpty', handleEmptyMedium);
    medium.on('joined', handleMediumJoin);
    medium.on('leaved', handleMediumLeave);

    joinRoom(connection, id);
    new Game(medium, connection.decoded_token.email);//todo: think about garbage collection

    broadcastRoom();

    function broadcastRoom() {
      var roomObject = makeRoomObject(medium);

      var connection;
      for (var id in connections) {
        connection = connections[id];

        roomObject.you = null; roomObject.joined = null;
        extendRoomObject(roomObject, connection);
        connection.emit('roomCreated', roomObject);//document
      }
    }

    function handleEmptyMedium(medium) {
      delete connectionMediums[id];
      broadcast('roomDestroyed', id);//document
    }

    function handleMediumJoin(connection) {
      broadcast('roomJoin', {
        id   : id,
        user : connection.decoded_token.email
      });//document
    }

    function handleMediumLeave(connection) {
      broadcast('roomLeave', {
        id   : id,
        user : connection.decoded_token.email
      });//document
    }
  }


  function handleJoinRoom(connection, id) {
    joinRoom(connection, id);
  }


  function handleLeaveRoom(connection, id) {
    leaveRoom(connection, id);
  }


  function joinRoom(connection, id) {
    if (connectionMediums[id]) {
      var medium = connectionMediums[id];

      if (connection.connectionMedium) {
        leaveRoom(connection, connection.connectionMedium.getId());
      }

      connection.connectionMedium = medium;
      medium.join(connection);
    }
  }


  function leaveRoom(connection, id) {
    if (connectionMediums[id]) {
      var medium = connectionMediums[id];

      connection.connectionMedium = null;
      medium.leave(connection);
    }
  }


  function handleDisconnect(connection) {
    if (connection.connectionMedium) {
      var medium = connection.connectionMedium;
      medium.leave(connection);
    }

    delete connections[connection.decoded_token.email];
  }


  function broadcast(event, data) {
    var connection;
    for (var id in connections) {
      connection = connections[id];
      connection.emit(event, data);
    }
  }

}

/*Class*/
function ConnectionMedium() {
  var that = this;

  var id;
  var connections = {};

  (function __construct() {
    /*extend*/
    events.EventEmitter.call(that);

    id = uuid.v1();

    /*public methods*/
    that.getId       = getId;
    that.getUserList = getUserList;
    that.join        = join;
    that.leave       = leave;
    that.emitTo      = emitTo;
    that.broadcast   = broadcast;
    that.__destruct  = __destruct;
    /**/

  })();


  function __destruct() {
    for (var id in connections) {
      leave(connections[id]);
    }
  }


  function getId() {
    return id;
  }


  function join(connection) {
    connections[connection.decoded_token.email] = connection;

    connection.on('communicate', handleCommunicate.bind(null, connection));//document
    connection.on('chat', handleChat.bind(null, connection));//document
    connection.on('getUserList', handleGetUserList.bind(null, connection));//document

    emitTo(connection, 'youJoined', getUserList());//document
    broadcast('joined', connection.decoded_token.email);//document

    that.emit('joined', connection);//document
  }


  function leave(connection) {
    connection.removeListener('communicate', handleCommunicate);
    connection.removeListener('chat', handleChat);

    delete connections[connection.decoded_token.email];
    broadcast('leaved', connection.decoded_token.email);//document
    that.emit('leaved', connection);//document

    if (Object.keys(connections).length === 0) {
      that.emit('mediumEmpty', that);//document
    }
  }


  function getUserList() {
    var result = {};

    for (var id in connections) {
      result[connections[id].decoded_token.email] = {};
    }

    return result;
  }


  function emitTo(id, event, data) {    
    var connection = connections[id];
    
    if (connection) {
      connection.emit(event, data);
    }
  }


  function broadcast(event, data) {
    for (var id in connections) {
      emitTo(id, event, data);
    }
  }


  function handleCommunicate(connection, data) {    
    if (typeof data.event !== 'undefined' && typeof data.payload !== 'undefined') {
      that.emit('communicate', {
        user : connection.decoded_token.email,
        data : data
      });
    }
  }


  function handleChat(connection, data) {
    broadcast('chat', {
      author : connection.decoded_token.email,
      data   : data
    });
  }


  function handleGetUserList(connection) {
    connection.emit('userList', getUserList());
  }

}

ConnectionMedium.super_    = events.EventEmitter;
ConnectionMedium.prototype = Object.create(events.EventEmitter.prototype, {
  constructor: {
    value      : ConnectionMedium,
    enumerable : false
  }
});

module.exports = ConnectionsManager;
