var server      = require('http').createServer()
var io          = require('socket.io')(server);
var socketioJwt = require('socketio-jwt');

var ConnectionsManager = require('./ConnectionsManager.js');
var connectionsManager = new ConnectionsManager();


io.sockets
.on('connection', socketioJwt.authorize({
  secret  : '76AU4nD8CXnGZMYr2HhRI5zQwxG7OX6n',
  timeout : 15000
}))
.on('authenticated', onAuthenticated);

server.listen(3002, function () {
  console.log('listening on http://localhost:3002');
});

function onAuthenticated(socket) {
  console.log('hello! ' + socket.decoded_token.email);
  handleSocketEvents(socket);
}

function handleSocketEvents(socket) {
  connectionsManager.addConnection(socket);
}
