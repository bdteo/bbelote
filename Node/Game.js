var extend = require('util')._extend;

/*Class*/
function Game(connectionMedium, gameOwner) {
  /*if (!connectionMedium instanceof ConnectionMedium) {
    throw 'connectionMedium argument is not instanceof ConnectionMedium!';
  }*/

  var order = [];
  var deck;
  var handPocessor;
  var cardsManager;

  var owner = gameOwner;

  (function __construct() {
    init();
    /*public methods*/
  })();


  function init() {
    deck         = new Deck();
    handPocessor = new HandProcessor();
    //////
      handPocessor.setTrumpSuits({});

    cardsManager = new PlayerCardsManager(deck, handPocessor, connectionMedium);

    connectionMedium.on('communicate', function (data) {
      var user    = data.user;
      var event   = data.data.event;
      var payload = data.data.payload;

      if (user === owner) {
        handleOwnerActions(event, payload);
      }
    });
  }


  function handleOwnerActions(event, payload) {
    if (event === 'startGame') {
      startGame();
    }
  }


  function startGame() {
    order = Object.keys(connectionMedium.getUserList());//in order to guarantee order
    cardsManager.init(order);
    cardsManager.dealOutThree();
    cardsManager.dealOutTwo();
    cardsManager.dealOutThree();

    var iterator = PlayerIterator(order, connectionMedium, 'playCard', function (nextPlayer, data) {
      var user = data.user;
      var card = data.data.payload;
      cardsManager.playCard(user, card);

      connectionMedium.emitTo(nextPlayer, 'yourTurn');//document
    })
  }

}


/*Class*/
function PlayerIterator(order, connectionMedium, event, callback) {
  var that  = this;

  var onTurn   = null,
      onTurnId = null;

  (function __construct() {
    onTurnId = 0;
    onTurn   = order[onTurnId];
    
    connectionMedium.on('communicate', advance);

    /*public methods*/
    that.__destruct = __destruct;
  })();


  function __destruct() {
    connectionMedium.removeListener('communicate', advance);
  }


  function advance(data) {
    if (
			data.data.event === event
			&& data.user === onTurn
		) {
      onTurnId = onTurnId + 1 < order.length
        ? onTurnId + 1
        : 0;
			
			console.log('onTurn', onTurn);
			
      onTurn = order[onTurnId];
			callback(onTurn, data);
    }
  }
}


/*Class*/
function PlayerCardsManager(deck, handPocessor, connectionMedium) {
  var that = this;
  var playerCards = {};
  var hands = [];
  var order;

  (function __construct() {
    /*public methods*/
    that.init         = init;
    that.dealOutTwo   = dealOutTwo;
    that.dealOutThree = dealOutThree;
    that.playCard     = playCard;
  })();


  function init(ord) {
    order = ord;

    playerCards = {};

    for (var i = 0, l = order.length; i<l; i++) {
      playerCards[order[i]] = {};
    }

    deck.resetCardAttributes();
    deck.shuffle();
  }


  function playCard(player, card) {
    var cardKey = card.rank + card.suit;
    
    console.log(cardKey, player);/////////
    
    if (cardKey in playerCards[player]) {
      var topHand = getTopHand();

      if (topHand && topHand.length === order.length) {//hand full				
        var handWinner = handPocessor.playHand(topHand);
        connectionMedium.broadcast('handWinner', handWinner);//document
        
        hands.push([]);
        topHand = getTopHand();
      }

      if (
        !topHand
        || topHand.length === order.length
      ) {
        hands.push([]);
        topHand = getTopHand();
      }

      var cardToPlay = playerCards[player][cardKey];
      delete playerCards[player][cardKey];
      topHand.push(cardToPlay);
      
      connectionMedium.broadcast('cardPlayed', {
				player    : player,
				card      : card,
				order     : order 
			});
    }
  }


  function getTopHand() {
    if (hands.length) {
      return hands[hands.length - 1]
    }
    else {
      return null;
    }
  }


  function dealOutTwo() {
    var player;

    for (var i = 0, l = order.length; i<l; i++) {
      player = order[i];
      dealOutCard(player);
      dealOutCard(player);
    }
  }


  function dealOutThree() {
    var player;

    for (var i = 0, l = order.length; i<l; i++) {
      player = order[i];
      dealOutCard(player);
      dealOutCard(player);
      dealOutCard(player);
    }
  }


  function dealOutCard(player) {
    var card = deck.pop();

    if (card) {      
      card.player = player;
      card.winner = null;
      var cardKey = card.getRank() + card.getSuit();

      playerCards[player][cardKey] = card;
      
      var cardObject = card.getPlainObject(),
          cardNum    = Object.keys(playerCards[player]).length;
      
      cardObject.num = cardNum;
      
      connectionMedium.emitTo(player, 'receiveCard', cardObject);//document
      connectionMedium.broadcast('receivesCard', {
        player : player,
        len    : cardNum
      });//document
    }

  }
}


/*Class*/
function HandProcessor() {
  var that = this;

  var trumpSuits = {};
  var allTrumps = null,
      noTrumps  = null,
      oneTrump  = null;

  var plainValues = {
    "7"  : { order : 1, value : 0  },
    "8"  : { order : 2, value : 0  },
    "9"  : { order : 3, value : 0  },
    "J"  : { order : 4, value : 2  },
    "Q"  : { order : 5, value : 3  },
    "K"  : { order : 6, value : 4  },
    "10" : { order : 7, value : 10 },
    "A"  : { order : 8, value : 11 }
  };

  var trumpValues = {
    "7"  : { order : 1, value : 0  },
    "8"  : { order : 2, value : 0  },
    "9"  : { order : 3, value : 14 },
    "J"  : { order : 4, value : 20 },
    "Q"  : { order : 5, value : 3  },
    "K"  : { order : 6, value : 4  },
    "10" : { order : 7, value : 10 },
    "A"  : { order : 8, value : 11 }
  };


  (function __construct() {
    /*public methods*/
    that.setTrumpSuits = setTrumpSuits;
    that.playHand      = playHand;
  })();


  function setTrumpSuits(suits) {
    trumpSuits = extend({}, suits);

    trumpSuitsArr = Object.keys(trumpSuits);

    allTrumps = trumpSuitsArr.length === 4;
    noTrumps  = trumpSuitsArr.length === 0;
    oneTrump  = trumpSuitsArr.length === 1;
  }


  function playHand(hand) {
    var majorSuit = hand[0].getSuit();
    var majorRank = hand[0].getRank();

    var possibleWinners = hand.filter(function (card) {
      return card.getSuit() === majorSuit
        || (oneTrump && cardIsTrump(card));
    });

    var sortedFilteredHand = possibleWinners.sort(suitBasedComparator);

    var winner = sortedFilteredHand[0].player;

    for (var i = 0, l = hand.length; i<l; i++) {
      hand[i].winner = winner;
    }

    return winner;
  }


  function suitBasedComparator(card1, card2) {
    if (cardIsTrump(card1) && !cardIsTrump(card2)) {
      return 1;
    }

    if (!cardIsTrump(card1) && cardIsTrump(card2)) {
      return -1;
    }

    if (
      card1.getSuit() !== card2.getSuit()
      || card1.getRank() !== card2.getRank()
    ) {
      return null;
    }

    var card1Value, card2Value;

    card1Value = cardIsTrump(card1)
      ? trumpValues[card1.getRank()]
      : plainValues[card1.getRank()];

    card2Value = cardIsTrump(card2)
      ? trumpValues[card2.getRank()]
      : plainValues[card2.getRank()];

    return card1Value.order > card2Value.order
      ? 1
      : -1;
  }


  function cardIsTrump(card) {
    return card.getSuit() in trumpSuits;
  }

}

/*Class*/
function Deck() {
  var that  = this;
  var deck  = [];

  var suits = {
    "clubs"   : 1,
    "diamonds" : 2,
    "hearts"   : 3,
    "spades"   : 4
  };

  var ranks = {
    "7"  : 1,
    "8"  : 2,
    "9"  : 3,
    "J"  : 4,
    "Q"  : 5,
    "K"  : 6,
    "10" : 7,
    "A"  : 8
  };

  (function __construct() {
    init();

    /*public methods*/
    that.pop     = pop;
    that.push    = push;
    that.shuffle = shuffle;
    that.resetCardAttributes = resetCardAttributes;
  })();


  function init() {
    deck = [];

    for (var suit in suits) {
      for (var rank in ranks) {
        deck.push(new Card(rank, suit));
      }
    }
  }


  function resetCardAttributes() {
    var card;
    for (var i = 0, l = deck.length; i<l; i++) {
      card = deck[i];
      card.player = null;
      card.winner = null;
    }
  }


  function shuffle() {
    deck = shuffleFisherYates(deck);
  }


  function push(card) {
    return deck.push();
  }


  function pop() {
    return deck.pop();
  }

}


/*Class*/
function Card(rank, suit) {
  var that = this;

  (function __construct() {
    /*public methods*/
    that.getRank = getRank;
    that.getSuit = getSuit;
    that.getPlainObject = getPlainObject;

    that.player  = null;
    that.winner  = null;
  })();


  function getRank() {
    return rank;
  }


  function getSuit() {
    return suit;
  }


  function getPlainObject() {
    return {
      rank : rank,
      suit : suit
    };
  }

}

function shuffleFisherYates(array) {
  var counter = array.length, temp, index;

  // While there are elements in the array
  while (counter > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
  }

  return array;
}

module.exports = Game;
